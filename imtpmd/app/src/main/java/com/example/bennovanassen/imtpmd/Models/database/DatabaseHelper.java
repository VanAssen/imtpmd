package com.example.bennovanassen.imtpmd.Models.database;

/**
 * Created by Benno on 5/22/2016.
 */

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// package <<Je eigen naam>>;           // import “STUFF” -  Gebruik alt+enter om je libs te importeren

public class DatabaseHelper extends SQLiteOpenHelper {
    public static SQLiteDatabase mSQLDB;
    private static DatabaseHelper mInstance;			// SINGLETON TRUC
    public static final String dbName = "barometer.db";	// Naam van je DB
    public static final int dbVersion = 1;				// Versie nr van je db.

    public DatabaseHelper(Context ctx) {				// De constructor doet niet veel meer dan ...
        super(ctx, dbName, null, dbVersion);			// … de super constructor aan te roepen.
    }

    public static synchronized DatabaseHelper getHelper (Context ctx){  // SYNCRONIZED TRUC
        if (mInstance == null){
            mInstance = new DatabaseHelper(ctx);
            mSQLDB = mInstance.getWritableDatabase();
        }
        return mInstance;
    }

    @Override // CREATE TABLE course (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, ects TEXT, code TEXT grade TEXT);
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DatabaseInfo.CourseTables.COURSE + " (" +
                BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseInfo.CourseColumn.NAME + " TEXT," + DatabaseInfo.CourseColumn.ECTS + " TEXT," +
                DatabaseInfo.CourseColumn.CODE + " TEXT," + DatabaseInfo.CourseColumn.GRADE + " TEXT);"
        );
    }    // . . . . continued on next slide
