package com.example.bennovanassen.imtpmd.Models.database;

/**
 * Created by Benno on 5/22/2016.
 */

public class DatabaseInfo {

    public class CourseTables {
        public static final String COURSE = "course";		// NAAM VAN JE TABEL
    }

    public class CourseColumn {
        public static final String NAME = "name";        // VASTE WAARDES
        public static final String ECTS = "ects";        // NAAM VAN DE KOLOMMEN
        public static final String GRADE = "grade";           // CIJFER
        public static final String PERIOD = "period";     // periode (1/2/3/4)
    }

}
